package net.sukharevd.cssw.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import net.sukharevd.cssw.controller.EmulateAction;
import net.sukharevd.cssw.controller.InitCommutativityAction;
import net.sukharevd.cssw.controller.NextAction;
import net.sukharevd.cssw.controller.OpenBracketsAction;
import net.sukharevd.cssw.controller.ParseAction;
import net.sukharevd.cssw.controller.PreviousAction;
import net.sukharevd.cssw.model.entities.AppContext;

/**
 * Frame of an application with all its components. It has the methods for
 * initialising the components.
 * 
 * @author <a href="mailto:sukharevd@gmail.com">Dmitriy Sukharev</a>
 * 
 */
/**
 * @author dmitriy
 *
 */
@SuppressWarnings("serial")
public class AppFrame extends JFrame {

    /**
     * Start width of the frame.
     */
    private static final int INIT_WIDTH = 1024;

    /**
     * Start height of the frame.
     */
    private static final int INIT_HEIGHT = 550;
    
    private AppContext context;
    
    private JTextField expression = new JTextField();
    private JTextArea notificationArea = new JTextArea();
    
    private JTabbedPane tabPane = new JTabbedPane();

    private JLabel statusLabel;

    /**
     * Constructor of AppFrame class, calls parent constructor and sets
     * <code>data</code> field.
     */
    public AppFrame(AppContext context) {
        this.context = context;
    }

    /**
     * Initialises and shows menu bar with all its items, initialises the event
     * handler for every component.
     */
/*    private void initializeMenuBar() {
        JMenuBar bar;

        JMenu file = new JMenu("File");
        JMenu help = new JMenu("Help");

        final int fontsize = 12;
        file.setFont(new Font("Lucida", 1, fontsize));
        help.setFont(new Font("Lucida", 1, fontsize));

        // -------------------------------------
        // ---------> File Menu <---------------
        // -------------------------------------

        JMenuItem open = new JMenuItem();
        JMenuItem save = new JMenuItem();
        JMenuItem generate = new JMenuItem();
        JMenuItem statistic = new JMenuItem();

        JMenuItem exit = new JMenuItem();

        open.setAction(new OpeningAction(this, data));
        open.setText("Open...");
        open.setMnemonic(KeyEvent.VK_O);
        open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));

        save.setAction(new SavingAction(this, data));
        save.setText("Save as...");
        save.setMnemonic(KeyEvent.VK_S);
        save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));

        generate.setAction(new GeneratingAction(this, data));
        generate.setText("Generate Set");
        generate.setMnemonic(KeyEvent.VK_G);
        generate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
        
        statistic.setAction(new StatisticAction(this));
        statistic.setText("Statistic...");
        statistic.setMnemonic(KeyEvent.VK_T);
        statistic.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
        //
        // export.setAction(new ExportingAction());
        // export.setText("Export...");
        // export.setMnemonic(KeyEvent.VK_E);
        // export.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,
        // ActionEvent.CTRL_MASK));
        //

        exit.setAction(new ExitingAction());
        exit.setText("Exit");
        exit.setMnemonic(KeyEvent.VK_Q);
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));

        // -------------------------------------
        // ---------> Help Menu <---------------
        // -------------------------------------

        // JMenuItem chelp = new JMenuItem();
        // JMenuItem about = new JMenuItem();
        //
        // chelp.setAction(new HelpingAction());
        // chelp.setText("Context Help...");
        // chelp.setMnemonic(KeyEvent.VK_F1);
        // chelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
        //
        // about.setAction(new AboutingAction());
        // about.setText("About...");
        // about.setMnemonic(KeyEvent.VK_B);
        // about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,
        // ActionEvent.CTRL_MASK));

        file.add(open);
        file.add(save);
        file.add(generate);
        file.add(statistic);
        // file.addSeparator();
        // file.add(export);
        file.add(exit);

        // help.add(chelp);
        // help.add(about);

        bar = new JMenuBar();
        bar.add(file);
        // bar.add(help);

        setJMenuBar(bar);
    }*/

    public JTextField getExpression() {
        return expression;
    }

    public void setExpression(JTextField expression) {
        this.expression = expression;
    }

    public JTextArea getNotificationArea() {
        return notificationArea;
    }

    public void setNotificationArea(JTextArea notificationArea) {
        this.notificationArea = notificationArea;
    }

    public JTabbedPane getTabPane() {
        return tabPane;
    }

    public JLabel getStatusLabel() {
        return statusLabel;
    }

    /**
     * Creates and shows components of GUI. Generates frame, menu, area for
     * drawing, etc.
     */
    public void createAndShowGUI() {
        setTitle("Computer System SoftWare");
        setSize(INIT_WIDTH, INIT_HEIGHT);
        setMinimumSize(new Dimension(INIT_WIDTH, INIT_HEIGHT));

        
//        initializeMenuBar();
        initialiseControlPanel();
        initialiseTabs();
        statusLabel = new JLabel();
        add(statusLabel, BorderLayout.SOUTH);        
        
        //initialiseNotificationPanel();

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        addWindowListener(new WindowAdapter() {
//            public void windowClosing(final WindowEvent we) {
//                System.exit(0);
//            }
//        });
    }
    
    // TODO change inPanel to inComponent, make method return JPanel, eliminate outPanel
    private JPanel panelToPanelWithScroll(JComponent inner) {
        final JFrame frame = this;
        AdjustmentListener listener = new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(@SuppressWarnings("unused") AdjustmentEvent e) {
                frame.repaint();
            }
        };
        JPanel outPanel = new JPanel(new BorderLayout());
        JScrollPane scroller = new JScrollPane(inner);
        scroller.getHorizontalScrollBar().addAdjustmentListener(listener);
        scroller.getVerticalScrollBar().addAdjustmentListener(listener);
        outPanel.add(scroller, BorderLayout.CENTER);
        return outPanel;
    }

    private void initialiseTabs() {
        initNotificationArea();
        JBinaryTree treePanel = new JBinaryTree(context);
        JPanel scrolledNotificationPanel = panelToPanelWithScroll(notificationArea);
        JPanel scrolledTreePanel = panelToPanelWithScroll(treePanel);
        tabPane.add("Notifications", scrolledNotificationPanel);
        tabPane.add("Tree", scrolledTreePanel);
        getContentPane().add(tabPane);
    }

    private void initNotificationArea() {
        notificationArea.setColumns(40);
        notificationArea.setRows(10);
        notificationArea.setEditable(false);
    }

    /**
     * Initialises and shows the control panel with all its items, initialises
     * the event handler for each component.
     */
    private void initialiseControlPanel() {
        JPanel controlPanel = new JPanel();

        JButton parseButton = new JButton(new ParseAction(this, context));
        parseButton.setText("Parse");
        parseButton.setToolTipText("Parse expression");

        JButton initCommutativityButton = new JButton(new InitCommutativityAction(this, context));
        initCommutativityButton.setText("Commutativity");
        initCommutativityButton.setToolTipText("Initiate Commutativity");
        
        JButton openBracketsButton = new JButton(new OpenBracketsAction(this, context));
        openBracketsButton.setText("Open Brackets");
        openBracketsButton.setToolTipText("Open Brackets");
        


        JButton nextButton = new JButton(new NextAction(this, context));
        nextButton.setText("Next");
        nextButton.setToolTipText("Show the next variant of the expression");

        JButton backButton = new JButton(new PreviousAction(this, context));
        backButton.setText("Back");
        backButton.setToolTipText("Show the previous variant of the expression");
                
        expression.setColumns(40);
        controlPanel.add(expression);
        controlPanel.add(parseButton);
        controlPanel.add(initCommutativityButton);
        controlPanel.add(openBracketsButton);

        controlPanel.add(nextButton);
        controlPanel.add(backButton);
        
        add(controlPanel, BorderLayout.NORTH);
    }

}
