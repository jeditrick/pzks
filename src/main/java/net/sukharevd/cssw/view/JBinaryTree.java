package net.sukharevd.cssw.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;

import net.sukharevd.cssw.model.entities.AppContext;
import net.sukharevd.cssw.model.entities.Node;

@SuppressWarnings("serial")
// TODO change JPanel to JComponent
public class JBinaryTree extends JComponent {
    
    private class Level {
        private List<Node> nodes = new ArrayList<Node>();
        public List<Node> getNodes() {
            return nodes;
        }
    }

    private AppContext context;
    private int maxX;
    private int maxY;
    private String lastExpression;

    public JBinaryTree(AppContext context) {
        this.context = context;
    }

    /** {@inheritDoc} */
    @Override
    public void paintComponent(final Graphics g) {
//        if (context.getRoot()!= null && context.getRoot().toString().equals(lastExpression.toString())) {
//            return;
//        }
//        lastExpression = context.getRoot().toString();
        positions.clear();
        levels.clear();
        cleanArea(g);

        Font f = new Font ("Lucida", Font.BOLD, 14);
        g.setFont (f);
        exploreLevels(context.getRoot(), 0);
        paintTree(g);
        setPreferredSize(new Dimension(maxX, maxY));
        revalidate();
    }

    private final int TOP_MARGIN = 20;
    private final int BOTTOM_MARGIN = 50;
    private final int NODE_RADIUS = 16;

    private void paintTree(Graphics g) {
        for (int i = 0; i < levels.size(); i++) {
            Level level = levels.get(i);
            paintLevel(g, i, level);
        }        
    }

    private Color GR1 = new Color(0xff, 0xff, 0xff);
    private Color GR2 = new Color(0xee, 0xee, 0xff);

    private void paintLevel(Graphics g, int i, Level level) {
        int nodesAmount = level.getNodes().size();
        int xSpace = getWidth() / (nodesAmount + 1);
        for (int j = 0; j < nodesAmount; j++) {
            int y = TOP_MARGIN + i * ((NODE_RADIUS << 1) + BOTTOM_MARGIN);
            int x = (j+1) * xSpace;
            g.setColor(Color.BLACK);
            paintLinks(g, x, y, i, j);
            paintNode(g, x, y, level.getNodes().get(j));
        }
    }

    private final int NODE_LEFT_PADDING = 5;
    private void paintNode(Graphics g, int x, int y, Node node) {
        Graphics2D g2 = (Graphics2D) g;
        GradientPaint gradient = new GradientPaint(
                x - NODE_RADIUS, y - NODE_RADIUS, GR1,
                x + NODE_RADIUS, y + NODE_RADIUS, GR2);
        g2.setPaint(gradient);
        g.fillOval(x - NODE_RADIUS, y - NODE_RADIUS, NODE_RADIUS << 1, NODE_RADIUS << 1);
        g.setColor(Color.RED);
        g.drawOval(x - NODE_RADIUS, y - NODE_RADIUS, NODE_RADIUS << 1, NODE_RADIUS << 1);
        g.drawString(node.getValue().toString(), x - NODE_RADIUS + NODE_LEFT_PADDING, y + 5);
        g.setColor(Color.LIGHT_GRAY);
        g.drawString(Integer.toString(node.weight()), x + NODE_RADIUS + 5, y + 5);
        g.drawString(Integer.toString(node.id), x - NODE_RADIUS - 25, y + 5);
        maxX = Math.max(maxX, x + NODE_RADIUS);
        maxY = Math.max(maxY, y + NODE_RADIUS);
    }

    private void paintLinks(Graphics g, int x1, int y1, int i, int j) {
        if (levels.size() > i + 1) {
            Node node = levels.get(i).getNodes().get(j);
            int nodesAmount = levels.get(i + 1).getNodes().size();
            int xSpace = getWidth() / (nodesAmount + 1);
            int y2 = TOP_MARGIN + (i + 1) * ((NODE_RADIUS << 1) + BOTTOM_MARGIN);
            paintLinkToChild(g, node.getLeftChild(), xSpace, x1, y1, y2);
            paintLinkToChild(g, node.getRightChild(), xSpace, x1, y1, y2);
        }
    }

    private void paintLinkToChild(Graphics g, Node child, int xSpace, int x1, int y1, int y2) {
        if (child != null) {
            int x2 = (positions.get(child)[1] + 1) * xSpace;
            g.drawLine(x1, y1, x2, y2);
        }        
    }

    /**
     * Cleans area to redraw points.
     * @param g2
     * @param minY minimum value of y
     * @param maxY maximum value of y
     */
    private void cleanArea(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(g2.getBackground());
        g2.fillRect(0, 0, this.getWidth(), this.getHeight());
        if (context.getRoot() == null) {
            g2.setColor(Color.blue);
            final String str = "There are no nodes for drawing.";
            final int symbolWidth = 3;
            g2.drawString(str, getWidth() / 2 - str.length() * symbolWidth, getHeight() / 2);
        }
    }


    private Map<Node, int[]> positions = new HashMap<Node, int[]>();
    private List<Level> levels = new ArrayList<Level>();
    public void exploreLevels(Node curNode, int curLevel) {
        if (curNode == null) {
            return;
        }
        if (curLevel >= levels.size()) {
            levels.add(new Level());
        }
        // add curNode
        levels.get(curLevel).getNodes().add(curNode);
        int posX = curLevel;
        int posY = levels.get(curLevel).getNodes().size() - 1;
        positions.put(curNode, new int[] {posX, posY});
        // take care about children
        exploreLevels(curNode.getLeftChild(), curLevel + 1);
        exploreLevels(curNode.getRightChild(), curLevel + 1);
    }
}
