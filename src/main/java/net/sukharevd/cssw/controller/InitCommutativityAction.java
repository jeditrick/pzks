package net.sukharevd.cssw.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;

import net.sukharevd.cssw.model.entities.AppContext;
import net.sukharevd.cssw.model.entities.BuiltNode;
import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.logic.ArExpParser;
import net.sukharevd.cssw.model.logic.exception.LexicalAnalysisException;
import net.sukharevd.cssw.model.logic.exception.SyntaxAnalysisException;
import net.sukharevd.cssw.view.AppFrame;

@SuppressWarnings("serial")
public class InitCommutativityAction extends AbstractAction {

    private AppFrame appFrame;
    private AppContext context;

    public InitCommutativityAction(AppFrame appFrame, AppContext context) {
        this.appFrame = appFrame;
        this.context = context;
    }

    @Override
    public void actionPerformed(@SuppressWarnings("unused") ActionEvent ev) {
        String str = appFrame.getExpression().getText();
        ArExpParser parser = new ArExpParser();
        try {
            Node root = parser.parse(str);
            if (root instanceof BuiltNode) {
                ((BuiltNode)root).sort();
                context.setSequences(((BuiltNode)root).getAllShuffledSequences());
                System.out.println(root);
                context.resetCurSequence();
                root = context.getSequences().get(context.nextSequence()).deepClone();
                appFrame.getStatusLabel().setText(context.getCurSequence() + ": " + root.toString().replace(",", ""));
                root = ((BuiltNode)root).glue();
                String variantsStr = getVariants(context.getSequences());
                System.out.println(variantsStr);
                appFrame.getNotificationArea().setText(variantsStr);
    //            appFrame.getNotificationArea().setText("OK");
                appFrame.getNotificationArea().setForeground(Color.BLACK);
                context.setRoot(root);
                appFrame.getTabPane().setSelectedIndex(1);
            }
        } catch (LexicalAnalysisException e) {
            appFrame.getNotificationArea().setText(e.getMessage());
            appFrame.getNotificationArea().setForeground(Color.RED);
            System.err.println(e.getMessage());
            appFrame.getTabPane().setSelectedIndex(0);
        } catch (SyntaxAnalysisException e) {
            appFrame.getNotificationArea().setText(e.getMessage());
            appFrame.getNotificationArea().setForeground(Color.RED);
            System.err.println(e.getMessage());
            appFrame.getTabPane().setSelectedIndex(0);
        }
        appFrame.repaint();
    }

    private String getVariants(List<Node> printedNodes) {
        StringBuilder sb = new StringBuilder("COMMUTATIVITY:\n");
        int i = 0;
        for (Node node : printedNodes) {
            sb.append(i++).append(": ").append(node).append('\n');
        }
        return sb.toString();
    }

}
