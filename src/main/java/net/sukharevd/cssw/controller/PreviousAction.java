package net.sukharevd.cssw.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import net.sukharevd.cssw.model.entities.AppContext;
import net.sukharevd.cssw.model.entities.BuiltNode;
import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.logic.MatrixSystemEmulator;
import net.sukharevd.cssw.view.AppFrame;

@SuppressWarnings("serial")
public class PreviousAction extends AbstractAction {

    private AppFrame appFrame;
    private AppContext context;

    public PreviousAction(AppFrame appFrame, AppContext context) {
        this.appFrame = appFrame;
        this.context = context;
    }

    @Override
    public void actionPerformed(@SuppressWarnings("unused") ActionEvent ev) {
        if (context.getSequences() != null) {
            Node root = context.getSequences().get(context.prevSequence()).deepClone();
            appFrame.getStatusLabel().setText(context.getCurSequence() + ": " + root.toString().replace(",", ""));
            System.out.println(context.getCurSequence() + ": " + root);
            root = ((BuiltNode) root).glue();
            
            final MatrixSystemEmulator em = new MatrixSystemEmulator();
            root.resetDone();
            em.execute(root);
            StringBuilder sb = new StringBuilder();
            String notification = sb.append("Levels amount: ").append(em.getLevelsAmount()).append("\nTs: " + em.getTs()).append("\nTp: " + em.getTp()).append("\nKy: " + em.getKy()).append("\nKe: " + em.getKe()).append("\nMax width:").append(em.getMaxWidth()).append("\nDiagram:\n").append(em.getGant()).toString();
            appFrame.getNotificationArea().setForeground(Color.BLACK);
            appFrame.getNotificationArea().setText(notification);
            
            context.setRoot(root);
            appFrame.repaint();
        } else {
            appFrame.getStatusLabel().setText("Please press Commutativity button first.");
        }
    }

}
