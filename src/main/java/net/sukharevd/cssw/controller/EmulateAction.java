package net.sukharevd.cssw.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;

import org.omg.CORBA.INTERNAL;

import net.sukharevd.cssw.model.entities.AppContext;
import net.sukharevd.cssw.model.entities.BuiltNode;
import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.logic.ArExpParser;
import net.sukharevd.cssw.model.logic.MatrixSystemEmulator;
import net.sukharevd.cssw.model.logic.exception.LexicalAnalysisException;
import net.sukharevd.cssw.model.logic.exception.SyntaxAnalysisException;


@SuppressWarnings("serial")
public class EmulateAction extends AbstractAction {


    protected String expression;

    public EmulateAction(String expression) {
        this.expression = expression;
    }

    public void actionPerformed() {
        String str = this.expression;
        System.out.println(str);
        ArExpParser parser = new ArExpParser();
        try {
            // Different weight of operations
            // a+b*d-e*(b-e)*(k+b*a)
            // a+b*d-e*(b-e)*(k+b)+x+a/a
            Node root = parser.parse(str);
            List<Node> variants = null;
            if (root instanceof BuiltNode) {
                variants = ((BuiltNode)root).openBrackets();
                variants.addAll(((BuiltNode)root).getAllShuffledSequences());
                deleteDuplications(variants);
                System.out.println("size:" + variants.size());
                int minTp = Integer.MAX_VALUE;
                Node best = null;
                MatrixSystemEmulator bestEm = null;
                for (Node node : variants) {
                    node = node.deepClone();
                    ((BuiltNode)node).sort();
                    System.out.println(node);
                    node = ((BuiltNode)node).glue();
                    node.resetDone();
                    // a+b*d-e*(b-e)*(k+b)
                    final MatrixSystemEmulator em = new MatrixSystemEmulator();
                    em.execute(node);
//                    System.out.println("Levels amount: " + em.getLevelsAmount());
//                    System.out.println("Ts: " + em.getTs());
//                    System.out.println("Tp: " + em.getTp());
//                    System.out.println("Ky: " + em.getKy());
//                    System.out.println("Ke: " + em.getKe());
                    if (minTp > em.getTp()) {
                        minTp = em.getTp();
                        best = node;
                        bestEm = em;
                    }
                }
                final MatrixSystemEmulator em = bestEm;
//                best = ((BuiltNode)best).sort();
//                best = ((BuiltNode)best).glue();
                //em.execute(best);
                StringBuilder sb = new StringBuilder();
                String notification = sb.append("Levels amount: ").append(em.getLevelsAmount()).append("\nTs: " + em.getTs()).append("\nTp: " + em.getTp()).append("\nKy: " + em.getKy()).append("\nKe: " + em.getKe()).append("\nMax width:").append(em.getMaxWidth()).append("\nDiagram:\n").append(em.getGant()).toString();
                System.out.println(notification);
            }

        } catch (LexicalAnalysisException e) {

        } catch (SyntaxAnalysisException e) {

        }
    }

    private void deleteDuplications(List<Node> variants) {
        for (int i = variants.size() - 1; i >= 0 ; i--) {
            for (int j = i - 1; j >= 0; j--) {
                if (variants.get(i).toOperationsString().equals(variants.get(j).toOperationsString())) {
                    variants.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
