package net.sukharevd.cssw.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;

import net.sukharevd.cssw.model.entities.AppContext;
import net.sukharevd.cssw.model.entities.BuiltNode;
import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.logic.ArExpParser;
import net.sukharevd.cssw.model.logic.exception.LexicalAnalysisException;
import net.sukharevd.cssw.model.logic.exception.SyntaxAnalysisException;
import net.sukharevd.cssw.view.AppFrame;

@SuppressWarnings("serial")
public class OpenBracketsAction extends AbstractAction {

    private AppFrame appFrame;
    private AppContext context;

    public OpenBracketsAction(AppFrame appFrame, AppContext context) {
        this.appFrame = appFrame;
        this.context = context;
    }

    @Override
    public void actionPerformed(@SuppressWarnings("unused") ActionEvent ev) {
        String str = appFrame.getExpression().getText();
        ArExpParser parser = new ArExpParser();
        try {
            Node root = parser.parse(str);
            List<Node> variants = null;
            if (root instanceof BuiltNode) {
                
                variants = ((BuiltNode)root).openBrackets();
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < variants.size() - 1; j++) {
                    sb.append(variants.get(j).toString().replaceAll("[,\\.]", "")).append(" = \n");
                }
                sb.append(variants.get(variants.size()-1).toString().replaceAll("[,\\.]", ""));
                appFrame.getNotificationArea().setText(sb.toString());
                
                root = variants.get(variants.size()-1);
                
                //System.out.println("Opened:" + ((BuiltNode) root).openOneBracketInNode());
                ((BuiltNode)root).sort();
                
                System.out.println(root);
                //root = ((BuiltNode) root).openOneBracketInNode();
                
                root = ((BuiltNode)root).glue();
                System.out.println(root);
            }
            appFrame.getStatusLabel().setText("B: " + root.toString().replace(",", ""));
//            appFrame.getNotificationArea().setText("OK");
            appFrame.getNotificationArea().setForeground(new Color(64, 150, 64));
            context.setRoot(root);
            context.setSequences(variants);
            appFrame.getTabPane().setSelectedIndex(1);
        } catch (LexicalAnalysisException e) {
            appFrame.getNotificationArea().setText(e.getMessage());
            appFrame.getNotificationArea().setForeground(Color.RED);
            System.err.println(e.getMessage());    
            appFrame.getTabPane().setSelectedIndex(0);
        } catch (SyntaxAnalysisException e) {
            appFrame.getNotificationArea().setText(e.getMessage());
            appFrame.getNotificationArea().setForeground(Color.RED);
            System.err.println(e.getMessage());
            appFrame.getTabPane().setSelectedIndex(0);
        }
        appFrame.repaint();
    }

}
