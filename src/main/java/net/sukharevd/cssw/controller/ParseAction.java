package net.sukharevd.cssw.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import net.sukharevd.cssw.model.entities.AppContext;
import net.sukharevd.cssw.model.entities.BuiltNode;
import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.logic.ArExpParser;
import net.sukharevd.cssw.model.logic.BTreePrinter;
import net.sukharevd.cssw.model.logic.exception.LexicalAnalysisException;
import net.sukharevd.cssw.model.logic.exception.SyntaxAnalysisException;
import net.sukharevd.cssw.view.AppFrame;

@SuppressWarnings("serial")
public class ParseAction extends AbstractAction {

    private AppFrame appFrame;
    private AppContext context;

    public ParseAction(AppFrame appFrame, AppContext context) {
        this.appFrame = appFrame;
        this.context = context;
    }

    @Override
    public void actionPerformed(@SuppressWarnings("unused") ActionEvent ev) {
        String str = appFrame.getExpression().getText();
        ArExpParser parser = new ArExpParser();
        try {
            Node root = parser.parse(str);
            if (root instanceof BuiltNode) {
                ((BuiltNode)root).sort();
                //System.out.println(root);
                root = ((BuiltNode)root).glue();
                //BTreePrinter.printNode(root);
                //System.out.println(root);
            }
            appFrame.getStatusLabel().setText("B: " + root.toString().replace(",", ""));
//            appFrame.getNotificationArea().setText("OK");
            appFrame.getNotificationArea().setForeground(new Color(64, 150, 64));
            context.setRoot(root);
            context.setSequences(null);
            appFrame.getTabPane().setSelectedIndex(1);
        } catch (LexicalAnalysisException e) {
            appFrame.getNotificationArea().setText(e.getMessage());
            appFrame.getNotificationArea().setForeground(Color.RED);
            System.err.println(e.getMessage());    
            appFrame.getTabPane().setSelectedIndex(0);
        } catch (SyntaxAnalysisException e) {
            appFrame.getNotificationArea().setText(e.getMessage());
            appFrame.getNotificationArea().setForeground(Color.RED);
            System.err.println(e.getMessage());
            appFrame.getTabPane().setSelectedIndex(0);
        }
        appFrame.repaint();
    }

}
