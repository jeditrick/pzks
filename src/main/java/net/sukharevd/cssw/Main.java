package net.sukharevd.cssw;

import javafx.beans.binding.StringExpression;
import net.sukharevd.cssw.controller.EmulateAction;
import net.sukharevd.cssw.controller.InitCommutativityAction;
import net.sukharevd.cssw.model.entities.AppContext;
import net.sukharevd.cssw.model.entities.BuiltNode;
import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.logic.ArExpParser;
import net.sukharevd.cssw.model.logic.BTreePrinter;
import net.sukharevd.cssw.model.logic.exception.LexicalAnalysisException;
import net.sukharevd.cssw.model.logic.exception.SyntaxAnalysisException;
import net.sukharevd.cssw.view.AppFrame;

import java.awt.*;
import java.util.List;
import java.util.Scanner;

// (a + 3.79  + (-a89)) + (80.0 + b) 
public class Main {

    public interface OperationsWeights{
        int PLUS = 1;
        int MINUS = 1;
        int MUL = 2;
        int DIV = 4;
    }

    private static void getAlternatives(String expression){
        ArExpParser parser = new ArExpParser();
        try {
            Node root = parser.parse(expression);
            if (root instanceof BuiltNode) {
                ((BuiltNode)root).sort();
                StringBuilder sb = new StringBuilder("COMMUTATIVITY:\n");
                int i = 0;
                for (Node node : ((BuiltNode)root).getAllShuffledSequences()) {
                    sb.append(i++).append(": ").append(node).append('\n');
                }
                System.out.println(expression);
                System.out.println(sb);
            }
        } catch (LexicalAnalysisException e) {
            System.err.println(e.getMessage());
        } catch (SyntaxAnalysisException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void printParallelTree(String expression){
        ArExpParser parser = new ArExpParser();
        try {
            Node root = parser.parse(expression);
            if (root instanceof BuiltNode) {
                ((BuiltNode)root).sort();
                root = ((BuiltNode)root).glue();
                System.out.println(expression);
                BTreePrinter.printNode(root);
            }
        } catch (LexicalAnalysisException e) {
            System.err.println(e.getMessage());
        } catch (SyntaxAnalysisException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        // a+b*d-e*(b-e)*(k+b*a)
        // a+b*d-e*(b-e)*(k+b)+x+a/a
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your expression:");
        String expression = sc.nextLine();
        EmulateAction emulateAction =new EmulateAction(expression);
        emulateAction.actionPerformed();
    }
}
