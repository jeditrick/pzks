package net.sukharevd.cssw.model.entities;

public class Lexeme {
    public static enum Type { CONST, VAR, OPERATOR, OP_BR, CL_BR, UNAR };

    private Type type;
    int place;
    StringBuilder val;
    public Lexeme(Type type, char first, int place) {
        this.type = type;
        this.place = place;
        this.val = new StringBuilder();
        this.val.append(first);
    }
    
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getVal() {
        return val.toString();
    }

    public void setVal(String val) {
        this.val = new StringBuilder(val);
    }

    public void addChar(char next) {
        val.append(next);
    }

    @Override
    public String toString() {
        return type.toString() + ":" + val;
    }
}
