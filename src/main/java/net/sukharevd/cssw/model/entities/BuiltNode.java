package net.sukharevd.cssw.model.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.sukharevd.cssw.model.logic.Shuffler;

public class BuiltNode extends Node {
    
    private static final boolean SKIP_PERMUTATION_IF_ONLY_TWO = false;
    private static final boolean FORBID_NEGATIVE_EXPRESSION_START = true;


    public BuiltNode() {
        super(Node.Type.BUILT, null);
    }

    private LinkedList<Node> list = new LinkedList<Node>();

    public void add(Node node) {
        list.add(node);

    }
    // (-a)+b+(c*(a-b-d))
    // a-72*b/(7*b/d)-b*d+7*(a+d)
    // a-a*a*a +a/(b-c) + a*a*a*a
    // (a+b)*c+2+7/x+m+n*s-y+(f-2+z)+e
    // a+b+c*d+e*k
    
    // (-a)+b+(c*(a-b-d))-a-72*b/(7*b/d)-b*d+7*(a+d)
    // a+b*(a+b)+c*3/32
    public List<Node> getAllShuffledSequences() {
        Shuffler shuffler = new Shuffler();
        
        List<Integer> shuttledTermsList = getShuffledTermsList();
        int shuffledTermsAmount = shuttledTermsList.size();
        List<Integer> numbers = new ArrayList<Integer>(shuffledTermsAmount);
        for (int i = 0; i < shuffledTermsAmount; i++) {
            numbers.add(i);
        }
        //System.out.println(numbers);
        List<List<Integer>> resultSequences = new ArrayList<List<Integer>>();
        shuffler.shuffle(new ArrayList<Integer>(), numbers, resultSequences, shuttledTermsList, list, 0);
        
        //System.out.println("resultSequences: " + resultSequences);
        Map<Integer, List<Node>> subsequences = new HashMap<Integer, List<Node>>();
        for (int i = 0; i < shuffledTermsAmount; i++) {
            if (list.get(shuttledTermsList.get(i)) instanceof BuiltNode) {
                BuiltNode subnode = (BuiltNode) list.get(shuttledTermsList.get(i));
                List<Node> nodes = subnode.getAllShuffledSequences();
                //System.out.println("subnodes: " + nodes);
                subsequences.put(i, nodes);
            } else {
                subsequences.put(i, Arrays.asList(list.get(shuttledTermsList.get(i))));
            }
        }
        
        List<Node> resultSequence = assembleSequences(shuttledTermsList, resultSequences, subsequences);
        filterSequences(resultSequence);
        //System.out.println(subsequences);
        return resultSequence;
    }

    private void filterSequences(List<Node> sequence) {
        for (int i = sequence.size() - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (sequence.get(i).toOperationsString().equals(sequence.get(j).toOperationsString())) {
                    sequence.remove(i);
                    break;
                }
            }
        }
    }
    private List<Node> assembleSequences(List<Integer> shuttledTermsIndexes, List<List<Integer>> resultSequences,
            Map<Integer, List<Node>> subsequences) {
        List<Node> nodes = new ArrayList<Node>();
        //System.out.println("resultSequences.size(): " + resultSequences.size());
        int max = resultSequences.size();
        max = (max == 2 && SKIP_PERMUTATION_IF_ONLY_TWO) ? 1 : max;
        for (int i = 0; i < max; i++) {
            List<Node> resultNodesSequencesForI = new ArrayList<Node>();
            List<Node> curNodesSequence = new ArrayList<Node>();
            getAllNodesFor(i, 0, shuttledTermsIndexes, resultSequences, subsequences, curNodesSequence, resultNodesSequencesForI);
//            for (Node n: resultNodesSequencesForI){
//                System.out.println(n + " " + weight());
//            }
            nodes.addAll(resultNodesSequencesForI);
//            BuiltNode.Builder builder = new BuiltNode.Builder(shuttledTermsIndexes, resultSequences, subsequences);
//            for (int j = 0; j < resultSequences.get(i).size(); j++) {
//                int shuttledIdx = resultSequences.get(i).get(j);;
//                for (int k = 0; k < subsequences.get(shuttledIdx).size(); k++) {
//                    builder.add(i,j,k);
//                }
//            }
//            nodes.add(builder.build());
//            System.out.println("Node: " + nodes.get(nodes.size()-1));
        }
        return nodes;
    }
    private void getAllNodesFor(final int i, int j, final List<Integer> shuttledTermsIndexes,
            final List<List<Integer>> resultSequences, final Map<Integer, List<Node>> subsequences, List<Node> curNodesSequence,
            List<Node> resultNodesSequences) {
        if (j == resultSequences.get(i).size()) {
            BuiltNode buildNode = new BuiltNode();
            for (Node node : curNodesSequence) {
                buildNode.add(node);
            }
            resultNodesSequences.add(buildNode);
            return;
        }
        int shuttledIdx = resultSequences.get(i).get(j);
//        Node prefix = getPrefixFor(shuttledIdx, shuttledTermsIndexes);
        for (int k = 0; k < subsequences.get(shuttledIdx).size(); k++) {
            int realIdx = shuttledTermsIndexes.get(shuttledIdx);
            ArrayList<Node> curClone = new ArrayList<Node>(curNodesSequence);
            Node nodeToBeAdded = subsequences.get(shuttledIdx).get(k);
            boolean isPositiveStart = addNodeWithItsSignAndOperation(nodeToBeAdded, realIdx, curClone);
            if (!isPositiveStart && FORBID_NEGATIVE_EXPRESSION_START) {
                curNodesSequence.clear();
                break;
            }
//            if (prefix != null) {
//                curClone.add(prefix);
//            }
//            curClone.add(subsequences.get(shuttledIdx).get(k));
            getAllNodesFor(i, j+1, shuttledTermsIndexes, resultSequences, subsequences, curClone, resultNodesSequences);
        }
    }
    private boolean addNodeWithItsSignAndOperation(Node nodeToBeAdded, int realIdx,
            ArrayList<Node> curClone) {
        boolean isPositiveStart = true;
        Node prefix = null;
        if (realIdx > 0) {
            prefix = list.get(realIdx - 1);
        } else if (realIdx < list.size() - 1){
            Node postfix = list.get(realIdx + 1);
            if ("+".equals(postfix.getValue()) || "-".equals(postfix.getValue()) ) {
                prefix = NodeFactory.createOp("+");
            } else if ("*".equals(postfix.getValue()) || "/".equals(postfix.getValue()) ) {
                prefix = NodeFactory.createOp("*");
            }
        }

        if (prefix != null) {
            if ("+".equals(prefix.getValue()) && nodeToBeAdded.getType() == Type.UNAR) {
                prefix = NodeFactory.createOp("-");
                nodeToBeAdded = nodeToBeAdded.getLeftChild();
            }
            
            if (curClone.isEmpty()) {
                if ("-".equals(prefix.getValue())) {
                    Node unarNodeToBeAdded = new Node(Type.UNAR, "-");
                    unarNodeToBeAdded.setLeftChild(nodeToBeAdded);
                    nodeToBeAdded = unarNodeToBeAdded;
                    isPositiveStart = false;
                } else if ("/".equals(prefix.getValue())) {
                    Node invertedNodeToBeAdded = NodeFactory.createOp("/");
                    Node one = new Node(Type.CONST, "1");
                    //invertedNodeToBeAdded.setLeftChild(one);
                    //invertedNodeToBeAdded.setRightChild(nodeToBeAdded);
                    //nodeToBeAdded = invertedNodeToBeAdded;
                    curClone.add(one);
                    curClone.add(invertedNodeToBeAdded);
                    isPositiveStart = false;
                }
            } else {
                curClone.add(prefix);
            }
        }
        curClone.add(nodeToBeAdded);
        return isPositiveStart;
    }
    private Node getPrefixFor(int idx, List<Integer> shuttledTermsIndexes) {
        //int idx = shuttledTermsIndexes.get(shuttledIdx);
        if (idx > 0) {
            return list.get(idx - 1);
        }
        return null;
    }
    private int countShuffledTerms() {
        int counter = 0;
        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i) instanceof BuiltNode) {
//                counter++;
//            } else
            if (list.get(i).getType() != Type.OP) {
                counter++;
            }
        }
        return counter;
    }
    
    private List<Integer> getShuffledTermsList() {
        List<Integer> shuttledTermsList = new ArrayList<Integer>(); 
//        int counter = 0;
        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i) instanceof BuiltNode) {
//                counter++;
//            } else
            if (list.get(i).getType() != Type.OP) {
                shuttledTermsList.add(i);
                //counter++;
            }
        }
        return shuttledTermsList;
    }

    public void sort() {
        boolean isSorted;
        do {
            isSorted = true;
            for (int i = -1; i < list.size() - 3; i += 2) {
                // boolean isPos = ("+".equals(list.get(1).getValue()) ||
                // "*".equals(list.get(1).getValue()));
                // boolean isGt = (list.get(0).weight() > list.get(2).weight());
                if (i == -1
                        && ("+".equals(list.get(1).getValue()) || "*"
                                .equals(list.get(1).getValue()))
                        && (list.get(0).weight() > list.get(2).weight())) {
                    Node temp = list.get(0);
                    list.set(0, list.get(2));
                    list.set(2, temp);
                } else if (i != -1
                        && list.get(i).weight() + list.get(i + 1).weight() > list.get(i + 2)
                                .weight() + list.get(i + 3).weight()) {
                    Node temp = list.get(i);
                    list.set(i, list.get(i + 2));
                    list.set(i + 2, temp);

                    temp = list.get(i + 1);
                    list.set(i + 1, list.get(i + 3));
                    list.set(i + 3, temp);
                    isSorted = false;
                }
            }
        } while (!isSorted);
        // balance
//        balance();
        // sort children
        for (Node node : list) {
            if (node instanceof BuiltNode) {
                ((BuiltNode) node).sort();
            }
        }
    }

    private void balance() {
        int wLast = list.getLast().weight();
        int wSum = 0;
        for (int i = 0; i < list.size() - 1; i++) {
            wSum += list.get(i).weight();
        }
        int wDelta = Math.abs(wLast - wSum);
        for (int i = list.size() - 5; i > 0; i -= 2) { // start from pre-last in the left part
            if (Math.abs(wLast - wSum + 2 * list.get(i).weight()) < wDelta) {
                exchange(i, list.size() - 3);
            }
        }
    }

    private void exchange(int i, int j) {
        list.add(j + 1, list.get(i));
        list.add(j + 1, list.get(i - 1));
        list.remove(i);
        list.remove(i - 1);
        // list.set(i, list.get(j));
        // list.set(j, temp);
        // temp = list.get(i - 1);
        // list.set(i - 1, list.get(j - 1));
        // list.set(j - 1, temp);
    }
    
//    public List<Node> openAllBrackets() {
//        if (list == null || list.isEmpty()) { throw new IllegalArgumentException(); }
//        
//        List<Node> variants = new ArrayList<Node>();
//        for (int i = 0; i < list.size(); i++) {
//            findBracketsInside(list.get(i), i, variants);
//        }
//        findBracketsInside(this, -1, variants);
//        return variants;
//    }
//
//    private void findBracketsInside(Node node, int i, List<Node> variants) {
//        boolean wasOpened = true;
//        Node openedNode = node;
//        while (wasOpened) {
//            if (openedNode.getType() == Type.BUILT) {
//            openedNode = ((BuiltNode) openedNode).openOneBracketInNode();
//            wasOpened = !openedNode.toOperationsString().equals(node.toOperationsString());
//            if (wasOpened && node != this) {
//                variants.add(getVariant(openedNode, i));
//            } else if (wasOpened && node == this) {
//                variants.add(openedNode);
//            }
//            }
//        }
//    }
//
//    private Node getVariant(Node node, int pos) {
//        BuiltNode resultNode = (BuiltNode) this.deepClone();
//        resultNode.list.set(pos, node);
//        return resultNode;
//    }
    
    public List<Node> openBrackets() {
        System.out.println("was: " + this);
        List<Node> variants = new ArrayList<Node>();
        BuiltNode lastVar = (BuiltNode) this.deepClone();
        variants.add(lastVar.deepClone());
        lastVar.sort();
        BuiltNode curVar = (BuiltNode) lastVar.openOneBracketInNode();
        variants.add(curVar.deepClone());
        int i = 1;
        while (!lastVar.toOperationsString().equals(curVar.toOperationsString())) {
            lastVar = (BuiltNode) curVar.deepClone();
            curVar = (BuiltNode) lastVar.openOneBracketInNode();
            System.out.println(i++ + ": " + curVar);
            variants.add(curVar.deepClone());
        }
        return variants;
    }

    public Node openOneBracketInNode() {
        if (list == null || list.isEmpty()) { throw new IllegalArgumentException(); }
// (a+b)/(c-d)*(e-f)
        deleteWrappingBrackets();
        deleteRedundantBrackets();
        
        for (int i = 0; i < list.size(); i+=2) {
            if (hasBracketAt(i)) {
                String prevOp = getOperatorAt(i - 1);
                String nextOp = getOperatorAt(i + 1);
                // (b-c)*(a-d)-(g-h)*k
                if (!isPriority(prevOp)) {
                    if (!isPriority(nextOp)) {
                        if ("-".equals(prevOp)) {
                            final Node invertSignAndEliminateBrackets = invertSignAndEliminateBrackets(i);
                            return invertSignAndEliminateBrackets;
                        } else if ("+".equals(prevOp)) {
                            return eliminateBrackets(i);
                        }
                    } else if (isPriority(nextOp)) {
                        if (i < list.size() - 1 && hasBracketAt(i+2)) {
                            if ("*".equals(nextOp)) {
                                return multiplyBrackets(i, i+2);
                            }
                        } else if (i < list.size() - 1) {
                            if ("*".equals(nextOp)) {
                                return multiplyBracketToValue(i, i+2);
                            }
                        }
                    }
                } else if (isPriority(prevOp)) {
//                    if ("*".equals(prevOp)) {
//                        int shift = 3; 
//                        while ("/".equals(getOperatorAt(i-shift)) && i - shift >= -1) { shift+=2;}
//                          if (i - shift >= -1) {
//                            return multiplyValueToBracket(i-shift+1, i);
//                          }
//                    }
                    
                    if ("*".equals(prevOp) && !"/".equals(getOperatorAt(i-3))) {
//                        if (!isPriority(nextOp)) {
                            return multiplyValueToBracket(i-2, i);
//                        }
                    } else if ("/".equals(prevOp)) {
                        if (isPriority(getOperatorAt(i-3)) && "/".equals(getOperatorAt(i-3))) {
                            return multiplyBrackets(i-2, i); // a/(b+d)/(k+e)
                        }
                    }
                }
            }
        }
        
        for (int i = 0; i < list.size(); i+=2) {
            if (list.get(i).getType() == Type.BUILT) {
                Node node = ((BuiltNode)list.get(i)).openOneBracketInNode();
                if (!node.toOperationsString().equals(list.get(i).toOperationsString())) {
                    BuiltNode res = (BuiltNode) this.deepClone();
                    res.list.set(i, node);
                    return res;
                }
            }
        }
        
        return this;
    }
    // improvement: a/b/c
    // bug: (a*b)*(c*d)
    // (a+b)*(c*d)*e
    // (a+b)*(c*(p+q))*e
    // (a+b)*(c*(p+q))

    private void deleteRedundantBrackets() {
        for (int i = 0; i < list.size(); i+=2) {
            if (list.get(i).getType() == Type.BUILT) {
                ((BuiltNode) list.get(i)).deleteRedundantBrackets();
//            if (hasBracketAt(i)) {
             // *null (a * b) */null
                final String prevOp = getOperatorAt(i - 1);
                final String nextOp = getOperatorAt(i + 1);
                final boolean isAppropriatePriorPrev = "*".equals(prevOp) || prevOp == null;
                final boolean isAppropriatePriorNext = isPriority(nextOp) || nextOp == null;
                final boolean isAppropriateUnpriorPrev = "+".equals(prevOp) || prevOp == null;
                final boolean isAppropriateUnpriorNext = !isPriority(nextOp) || nextOp == null;
                if ((isPriorityBracket(i) && isAppropriatePriorPrev && isAppropriatePriorNext)
                    || (!isPriorityBracket(i) && isAppropriateUnpriorPrev && isAppropriateUnpriorNext)) {
                    list.addAll(i + 1, ((BuiltNode)list.get(i)).list);
                    list.remove(i);
                    i-=2;
                }
                else
                if (!isPriorityBracket(i) && "-".equals(prevOp) && isAppropriateUnpriorNext) {
                    LinkedList<Node> bracketNodes = ((BuiltNode)list.get(i)).list;
                    for (int j = 1; j < bracketNodes.size(); j += 2) {
                        if (isPriority(bracketNodes.get(j).getValue().toString())) { throw new IllegalStateException("unpriority operation expected"); } 
                        bracketNodes.get(j).setValue(invertNotPriorityOperator(bracketNodes.get(j).getValue()).toString().trim());
                    }
                    list.addAll(i + 1, ((BuiltNode)list.get(i)).list);
                    list.remove(i);
                    i-=2;
                }
            }
        }
    }

    private void deleteWrappingBrackets() {
        if (list.size() == 1 && list.get(0).getType() == Type.BUILT) {
            list.addAll(((BuiltNode)list.get(0)).list);
            list.removeFirst();
        }
        for (int i = 0; i < list.size(); i += 2) {
            if (list.get(i).getType() == Type.BUILT) {
                ((BuiltNode)list.get(i)).deleteWrappingBrackets();
            }
        }
    }
    
    private boolean isPriorityBracket(int i) {
        BuiltNode bracket = (BuiltNode) list.get(i);
        for (int j = 1; j < bracket.list.size(); j+=2) {
            if (!isPriority(bracket.getOperatorAt(j))) {
                return false;
            }
        }
        return true;
    }

    private BuiltNode addOperatorToBracket(List<Node> nodes, BuiltNode lastNode, String op) {
        if (op != null) {
            if (isPriority(op)) {
                lastNode.add(new Node(Type.OP, op));
            } else {
                nodes.add(lastNode);
                lastNode = new BuiltNode();
                nodes.add(new Node(Type.OP, op));
            }
        }
        return lastNode;
    }

    private Node multiplyValueToBracket(int n1, int n2) {
        BuiltNode result = (BuiltNode) deepClone();
        Node node1 = result.list.get(n1);
        BuiltNode bracketNode2 = (BuiltNode) result.list.get(n2);
        
        List<Node> nodes = new LinkedList<Node>();
        BuiltNode lastNode = new BuiltNode();
        int i = n1;
            for (int j = 0; j < bracketNode2.list.size(); j+=2) {
                String op = bracketNode2.getOperatorAt(j - 1);
                lastNode = addOperatorToBracket(nodes, lastNode, op);

                lastNode.add(node1.deepClone());   // deepClone
                int oldI = i;
                while (i - 1 >= 0 && isPriority(result.list.get(i - 1).toString())) {
                    lastNode.add(new Node(Type.OP, result.list.get(i - 1).toString()));
                    lastNode.add(result.list.get(i - 2));
                    i-=2;
                }
                if (j < bracketNode2.list.size()-1) {
                    i = oldI;
                }
                
                lastNode.add(new Node(Type.OP, "*"));
                lastNode.add(bracketNode2.list.get(j));
                while (j + 1 < bracketNode2.list.size() && isPriority(bracketNode2.list.get(j + 1).toString())) {
                    lastNode.add(new Node(Type.OP, bracketNode2.list.get(j + 1).toString()));
                    lastNode.add(bracketNode2.list.get(j + 2));
                    j+=2;
                }
        }
        nodes.add(lastNode);

        i = n2;
        while (i - 1 >= 0 && isPriority(result.list.get(i - 1).toString())) {
            result.list.remove(--i);
            result.list.remove(--i);
        }
        ((BuiltNode)result.list.get(i)).list.clear(); // clear content of the second bracket
        ((BuiltNode)result.list.get(i)).list.addAll(nodes);
        result.list.get(i).setBracket(true);
        return result;
    }

    private Node multiplyBracketToValue(int n1, int n2) {
//        throw new UnsupportedOperationException();
        BuiltNode result = (BuiltNode) deepClone();
        
        BuiltNode bracketNode1 = (BuiltNode) result.list.get(n1);
        Node node2 = result.list.get(n2);
        
        List<Node> nodes = new LinkedList<Node>();
        BuiltNode lastNode = new BuiltNode();
        for (int i = 0; i < bracketNode1.list.size(); i+=2) {
                if (i > 0) {
                    String op = bracketNode1.getOperatorAt(i - 1);
                    lastNode = addOperatorToBracket(nodes, lastNode, op);
                }
                lastNode.add(bracketNode1.list.get(i).deepClone());   // deepClone
                lastNode.add(new Node(Type.OP, "*"));
                lastNode.add(node2.deepClone());   // deepClone
                int j = n2;
                while (j + 1 < result.list.size() && isPriority(result.list.get(j + 1).toString())) {
                    lastNode.add(new Node(Type.OP, result.list.get(j + 1).toString()));
                    lastNode.add(result.list.get(j + 2).deepClone());   // deepClone
                    j += 2;
                }
        }
        nodes.add(lastNode);
        
        int j = n2;
        while (j + 1 < result.list.size() && isPriority(result.list.get(j + 1).toString())) {
            result.list.remove(j+2);
            result.list.remove(j+1);
        }

        result.list.remove(n2);
        result.list.remove(n2-1);
        ((BuiltNode)result.list.get(n1)).list.clear(); // clear content of the second bracket
        ((BuiltNode)result.list.get(n1)).list.addAll(nodes);
        result.list.get(n1).setBracket(true);
        return result;
    }

    private Node multiplyBrackets(final int n1, final int n2) {
        BuiltNode result = (BuiltNode) deepClone();
        BuiltNode bracketNode1 = (BuiltNode) result.list.get(n1);
        BuiltNode bracketNode2 = (BuiltNode) result.list.get(n2);

        List<Node> nodes = new LinkedList<Node>();
        BuiltNode lastNode = new BuiltNode(); 
        for (int i = 0; i < bracketNode1.list.size(); i+=2) {
            for (int j = 0; j < bracketNode2.list.size(); j+=2) {
                if ((i | j) > 0) {
                    String n1Op = bracketNode1.getOperatorAt(i - 1);
                    String n2Op = bracketNode2.getOperatorAt(j - 1);
                    String op = multiplySigns(n1Op, n2Op);
                    lastNode = addOperatorToBracket(nodes, lastNode, op);
                }
                lastNode.add(bracketNode1.list.get(i).deepClone());     // deepClone
                int oldI = i;
                while (i + 1 < bracketNode1.list.size() && isPriority(bracketNode1.list.get(i + 1).toString())) {
                    lastNode.add(new Node(Type.OP, bracketNode1.list.get(i + 1).toString()));
                    lastNode.add(bracketNode1.list.get(i + 2).deepClone());     // deepClone
                    i+=2;
                }
                if (j < bracketNode2.list.size()-1) {
                    i = oldI;
                }
                lastNode.add(new Node(Type.OP, "*"));
                lastNode.add(bracketNode2.list.get(j).deepClone());     // deepClone
                while (j + 1 < bracketNode2.list.size() && isPriority(bracketNode2.list.get(j + 1).toString())) {
                    lastNode.add(new Node(Type.OP, bracketNode2.list.get(j + 1).toString()));
                    lastNode.add(bracketNode2.list.get(j + 2).deepClone());     // deepClone
                    j+=2;
                }
            }
        }
        nodes.add(lastNode);

        //LinkedList<Node> bracketNodes = bracketNode1.list;
        result.list.remove(n1); // remove first bracket
        result.list.remove(n1); // remove operator after first bracket
        ((BuiltNode)result.list.get(n1)).list.clear(); // clear content of the second bracket
        ((BuiltNode)result.list.get(n1)).list.addAll(nodes);
        result.list.get(n1).setBracket(true);
        return result;
    }

    private String multiplySigns(String n1Op, String n2Op) {
        if (n1Op == null) {             // TODO and what if n1Op and n2Op are * or / ???
            return n2Op;
        }
        if (n2Op == null) { // if there is no operator before first bracket
            return n1Op;
        }

        if (n1Op.equals(n2Op)) {
            return "+";
        } else {
            return "-";
        }
    }

    private Node eliminateBrackets(int i) {
        BuiltNode result = (BuiltNode) deepClone();
        BuiltNode bracketBuiltNode = (BuiltNode) result.list.get(i);
        LinkedList<Node> bracketNodes = bracketBuiltNode.list;
        result.list.remove(i);
        result.list.addAll(i, bracketNodes);
        return result;
    }

    private Node invertSignAndEliminateBrackets(int i) {
        BuiltNode result = (BuiltNode) deepClone();
        BuiltNode bracketBuiltNode = (BuiltNode) result.list.get(i);
        // from 1 cause first operand will get sign before bracket
        LinkedList<Node> bracketNodes = bracketBuiltNode.list;
        for (int j = 1; j < bracketNodes.size(); j += 2) {
            if (isPriority(bracketNodes.get(j).getValue().toString())) { throw new IllegalStateException("unpriority operation expected"); } 
            bracketNodes.get(j).setValue(invertNotPriorityOperator(bracketNodes.get(j).getValue()).toString().trim());
        }
        result.list.remove(i);
        result.list.addAll(i, bracketNodes);
        return result;
    }

    private boolean isPriority(String prevOp) {
        return "*".equals(prevOp) || "/".equals(prevOp);
    }

    private String getOperatorAt(int i) {
        if (i >= 0 && i < list.size()) {
            Node node = list.get(i);
            if (node.getType() == Type.OP) {
                return node.getValue().toString();
            } else {
                throw new IllegalStateException("operation expected");
            }
        } else {
            return null;
        }
    }

    private boolean hasBracketAt(int i) {
        if (list.size() > 1 && list.size() > i && list.get(i).getType() == Type.BUILT) {
            //String op = list.get(1).getValue().toString();
            return list.get(i).isBracket();
        }
        return false;
    }

    
    
    
    
    public Node glue() {
        // bind children
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) instanceof BuiltNode) {
                list.set(i, ((BuiltNode) list.get(i)).glue());
            } else if (list.get(i).getType() == Type.UNAR && list.get(i).getLeftChild().getType()==Type.BUILT) {
                list.get(i).setLeftChild(((BuiltNode) list.get(i).getLeftChild()).glue());
            }
        }
        // start glue current built node while only one (root) node leaves
        while (list.size() > 1) {
            for (int i = 0; i < list.size(); i += 4) {
                if (list.size() > 4 && i == list.size() - 3) {
                    if (countSumBefore(i) < list.getLast().weight()) {
                        glueWithLeftNode(i);
                    } else {
                        glueWithRightNode(i);
                    }
                } else if (list.size() > 6 && i == list.size() - 5) {
                    if (countSumBefore(i+1) < list.getLast().weight()) {
                        glueWithRightNode(i);
                    } else {
                        glueWithRightNode2(i+4);
                    }
                } else {
                    glueWithRightNode(i);
                }
            }
            // remove all odd nodes
            Iterator<Node> it = list.iterator();
            int pos = 1;
            while (it.hasNext()) {
                it.next();
                if ((pos & 2) != 0) {
                    it.remove();
                }
                pos++;
            }
        }
        list.get(0).setScopeSeparator(true);
        return list.get(0);
    }

    private int countSumBefore(int before) {
        int sum = 0;
        for (int i = 0; i < before; i += 4) {
            sum += list.get(i).weight();
        }
        return sum;
    }

    // a + b + c + d => ((a + b) + c) + d
    private void glueWithLeftNode(int curNode) {
        Node op = new Node(Node.Type.OP, list.get(curNode - 1).getValue());
        op.setLeftChild(list.get(curNode - 4));
        op.setRightChild(list.get(curNode));
        list.set(curNode - 4, op);
        list.set(curNode - 1, list.get(list.size() - 2));    // to save operator for the last operand
        list.set(curNode, list.getLast());
        invertOperatorIfNecessary(curNode -4, op);
    }

    private void glueWithRightNode2(int curNode) {
            Node op = new Node(Node.Type.OP, list.get(curNode - 1).getValue());
            op.setLeftChild(list.get(curNode - 2));
            op.setRightChild(list.get(curNode));
            list.set(curNode, op);
            invertOperatorIfNecessary(curNode, op);
    }
    
    private void glueWithRightNode(int curNode) {
        if (list.size() - 1 != curNode) {
            Node op = new Node(Node.Type.OP, list.get(curNode + 1).getValue());
            op.setLeftChild(list.get(curNode));
            op.setRightChild(list.get(curNode + 2));
            list.set(curNode, op);
            invertOperatorIfNecessary(curNode, op);
        }
    }

    private void invertOperatorIfNecessary(int firstNodeInBrackets, Node op) {
        // TODO wtf
        if (op.getRightChild() != null && !op.getRightChild().isScopeSeparator() && op.getValue().equals(op.getRightChild().getValue())) {
            op.getRightChild().setValue(invertNegativeOperator(op.getRightChild().getValue()));
        }
        // (A+B)+C/D+G+(K/L+M+N)-15+4
        if (firstNodeInBrackets <= 0) return;
        Node beforeBracket = list.get(firstNodeInBrackets - 1);
        if ("-".equals(beforeBracket.getValue()) || "/".equals(beforeBracket.getValue())) {
            //Node secondInBrackets = list.get(firstNodeInBrackets + 1);
            op.setValue(invertOperator(op.getValue()));
        }
    }
    // TODO wtf
    private Object invertOperator(Object value) {
        if ("-".equals(value)) {
            return "+";
        } else if ("/".equals(value)) {
            return "*";
        } else if ("+".equals(value)) {
            return "- ";
        } else if ("*".equals(value)) {
            return "/ ";
        }
        return value;
    }
    
    private Object invertNotPriorityOperator(Object value) {
        if ("-".equals(value)) {
            return "+";
        } else if ("+".equals(value)) {
            return "- ";
        }
        return value;
    }

    private Object invertNegativeOperator(Object value) {
        if ("-".equals(value)) {
            return "+";
        } else if ("/".equals(value)) {
            return "*";
        } else 
        return value;
    }

    @Override
    public int weight() {
        int sum = 0;
        for (Node node : list) {
            sum += node.weight();
        }
        return sum;
    }

    public boolean hasOne() {
        return list.size() == 1;
    }
    
    @Override
    public String toOperationsString() {
        StringBuilder sb = new StringBuilder("");
        for (Node node : list) {
            sb.append(node.toOperationsString());
        }
        return sb.append("").toString();
    }

    @Override
    public String toString() {
        return list.toString() + ((isBracket())? "." : "");
    }
    
    @Override
    public Node deepClone() {
        BuiltNode clone = new BuiltNode();
        clone.setType(getType());
        clone.setValue(getValue());
        clone.setParent(getParent());
        clone.setLeftChild(getLeftChild());
        clone.setRightChild(getRightChild());
        clone.setBracket(isBracket());
        for (Node node : list) {
            clone.add(node.deepClone());
        }
        return clone;
    }
}

final class NodeFactory {
    public static Node createConst(int curLexeme, List<Lexeme> list) {
        return new Node(Node.Type.CONST, list.get(curLexeme).getVal());
    }

    public static Node createOp(String value) {
        return new Node(Node.Type.OP, value);
    }

    public static Node createVar(int curLexeme, List<Lexeme> list) {
        return new Node(Node.Type.VAR, list.get(curLexeme).getVal());
    }

    public static Node createUnar(int curLexeme, List<Lexeme> list) {
        return new Node(Node.Type.UNAR, list.get(curLexeme++).getVal());
    }
}