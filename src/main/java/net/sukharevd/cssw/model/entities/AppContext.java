package net.sukharevd.cssw.model.entities;

import java.util.List;

public class AppContext {

    private Node root;
    private int curSequence;
    private List<Node> sequences;

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public List<Node> getSequences() {
        return sequences;
    }

    public void setSequences(List<Node> sequences) {
        this.sequences = sequences;
    }

    public int nextSequence() {
        this.curSequence++;
        if (curSequence >= sequences.size()) {
            this.curSequence = 0;
        }
        return curSequence;
    }

    public int prevSequence() {
        this.curSequence--;
        if (curSequence < 0) {
            this.curSequence = sequences.size() - 1;
        }
        return curSequence;
    }

    public void resetCurSequence() {
        curSequence = -1;        
    }

    public int getCurSequence() {
        return curSequence;
    }

}
