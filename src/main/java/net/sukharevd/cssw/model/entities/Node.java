package net.sukharevd.cssw.model.entities;

import net.sukharevd.cssw.Main;

public class Node {
    
    public static enum Type {VAR, CONST, OP, UNAR, BUILT}
    
    public int id;
    private Type type;
    private Node parent;
    private Node leftChild;
    private Node rightChild;
    private Object value;
    private boolean isScopeSeparator; // it's necessary for distinguishing a/b/c from a/(b/c), and only for this
    private boolean isBracket;
    public boolean isDone;
    
    public Node(Type type, Object value) {
        this.type = type;
        this.value = value;
    }


    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }
    public Node getParent() {
        return parent;
    }
    public void setParent(Node parent) {
        this.parent = parent;
    }
    public Node getLeftChild() {
        return leftChild;
    }
    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
        if (leftChild != null) {
            this.leftChild.setParent(this);
        }
    }
    public Node getRightChild() {
        return rightChild;
    }
    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
        if (rightChild != null) {
            this.rightChild.setParent(this);
        }
    }
    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }
    public boolean isScopeSeparator() {
        return isScopeSeparator;
    }


    public void setScopeSeparator(boolean isScopeSeparator) {
        this.isScopeSeparator = isScopeSeparator;
    }


    public boolean isBracket() {
        return isBracket;
    }


    public void setBracket(boolean isBracket) {
        this.isBracket = isBracket;
    }


    @Override
    public String toString() {
        if (type == Type.UNAR) {
            return value + getLeftChild().toString();
        }
        Node r = getRightChild();
        Node l = getLeftChild();
        return ((l == null)? "" : l.toString()) + value + ((r == null)? "" : r.toString());
//        return value.toString();
    }


    public int weight() {
        if (type == Type.OP) {
            int l = (leftChild == null) ? 0 : leftChild.weight();
            int r = (rightChild == null) ? 0 : rightChild.weight();
            if ("+".equals(value)) {
                return Main.OperationsWeights.PLUS + l + r;
            } else if ("-".equals(value) || "- ".equals(value)) {
                return Main.OperationsWeights.MINUS + l + r;
            } else if ("*".equals(value) || "/ ".equals(value)) {
                return Main.OperationsWeights.MUL + l + r;
            } else if ("/".equals(value)) {
                return Main.OperationsWeights.DIV + l + r;
            }
        } else if (type == Type.UNAR) {
            if (leftChild != null) {
                return leftChild.weight();
            }
        }
        return 0;
    }


    public String toOperationsString() {
        if (type == Type.OP) {
            return value.toString();
        } else if (type == Type.UNAR) {
            return leftChild.toOperationsString();
        } else {
            return "1";
        }
    }
    
    public Node deepClone() {
        Node clone = new Node(type, value);
        clone.setParent(parent);
        clone.setLeftChild(leftChild);
        if (type == Type.UNAR) {
            clone.setLeftChild(leftChild.deepClone());
        }
        clone.setRightChild(rightChild);
        return clone;
    }
    
    public void resetDone() {
        isDone = false;
        if (leftChild != null)
          leftChild.resetDone();
        if (rightChild != null)
          rightChild.resetDone();
    }
}
