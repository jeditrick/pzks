package net.sukharevd.cssw.model.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.sukharevd.cssw.model.entities.Lexeme;
import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.logic.exception.LexicalAnalysisException;
import net.sukharevd.cssw.model.logic.exception.SyntaxAnalysisException;

public class ArExpParser {

    public Node parse(String str) throws LexicalAnalysisException, SyntaxAnalysisException {
        LexicalAnalyser lexicalAn = new LexicalAnalyser();
        SyntaxAnalyser syntaxAn = new SyntaxAnalyser();
        List<Lexeme> lexems = lexicalAn.parse(str);
        return syntaxAn.syntaxAnalysis(lexems);
    }
}