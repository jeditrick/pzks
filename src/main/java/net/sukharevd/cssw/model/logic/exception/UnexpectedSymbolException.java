package net.sukharevd.cssw.model.logic.exception;

@SuppressWarnings("serial")
public class UnexpectedSymbolException extends LexicalAnalysisException {

    public UnexpectedSymbolException(char ch, int pos) {
        super("Illegal character '" + ch + "' at " + pos);
    }

}
