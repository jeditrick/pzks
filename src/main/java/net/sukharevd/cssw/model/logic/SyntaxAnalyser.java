package net.sukharevd.cssw.model.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.sukharevd.cssw.model.entities.BuiltNode;
import net.sukharevd.cssw.model.entities.Lexeme;
import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.logic.exception.MissingClosingBracketException;
import net.sukharevd.cssw.model.logic.exception.SyntaxAnalysisException;
import net.sukharevd.cssw.model.logic.exception.UnexpectedLexemeException;

public class SyntaxAnalyser {

    public Node syntaxAnalysis(List<Lexeme> list) throws SyntaxAnalysisException {
        curLexeme = 0;
        Node root = parseExpression(list);
        checkInTheEnd(list);
        return root;
    }
    
    private void checkInTheEnd(List<Lexeme> list) throws UnexpectedLexemeException {
        if (list.size() > curLexeme && !isOperator1(list.get(curLexeme))) {
            throw new UnexpectedLexemeException(list.get(curLexeme));
        }
    }

    private int curLexeme;

    private Node parseExpression(List<Lexeme> list) throws SyntaxAnalysisException {
        BuiltNode builtNode = new BuiltNode();
        Node node = parseTerm1(list);
        builtNode.add(node);
        while (list.size() > curLexeme && isOperator1(list.get(curLexeme))) {
            Node op = NodeFactory.createOp(curLexeme++, list);
            builtNode.add(op);
            //op.setLeftChild(node);
            node = parseTerm1(list);
            //op.setRightChild(node);
            builtNode.add(node);
            node = op;
        }
        if (builtNode.hasOne()) {
            return node;
        } else {
            return builtNode;
        }
    }

    private Node parseTerm1(List<Lexeme> list) throws SyntaxAnalysisException {
        BuiltNode builtNode = new BuiltNode();
        Node node = parseTerm2(list);
        builtNode.add(node);
        while (list.size() > curLexeme && isOperator2(list.get(curLexeme))) {
            Node op = NodeFactory.createOp(curLexeme++, list);
            //op.setLeftChild(node);
            builtNode.add(op);
            node = parseTerm2(list);
            //op.setRightChild(node);
            builtNode.add(node);
            node = op;
        }
        if (builtNode.hasOne()) {
            return node;
        } else {
            return builtNode;
        }
    }

    private Node parseTerm2(List<Lexeme> list) throws SyntaxAnalysisException {
        if (list.get(curLexeme).getType() == Lexeme.Type.CONST) {
            return NodeFactory.createConst(curLexeme++, list);
        } else if (list.get(curLexeme).getType() == Lexeme.Type.VAR) {
            return NodeFactory.createVar(curLexeme++, list);
        } else if (list.get(curLexeme).getType() == Lexeme.Type.OP_BR) {
            curLexeme++;
            return parseBrackets(list);
        }
//        curLexeme++;
        throw new UnexpectedLexemeException(list.get(curLexeme));
    }

    private Node parseBrackets(List<Lexeme> list) throws SyntaxAnalysisException {
        Node insideBr = null;
        if (list.get(curLexeme).getType() == Lexeme.Type.UNAR) {
            insideBr = NodeFactory.createUnar(curLexeme++, list);
            insideBr.setLeftChild(parseTerm2(list));
        } else {
            insideBr = parseExpression(list);
        }
        checkClBr(list);
        insideBr.setBracket(true);
        return insideBr;
    }

    private void checkClBr(List<Lexeme> list) throws MissingClosingBracketException {
        if (list.size() == curLexeme
                || list.get(curLexeme).getType() != Lexeme.Type.CL_BR) {
            throw new MissingClosingBracketException();
        }
        curLexeme++;
    }
    
    private boolean isOperator1(Lexeme lexeme) {
        List<String> operators1 = new ArrayList<String>(Arrays.asList("+", "-"));
        boolean isOperator = lexeme.getType() == Lexeme.Type.OPERATOR; 
        return isOperator && operators1.contains(lexeme.getVal());
    }
    
    private boolean isOperator2(Lexeme lexeme) {
        List<String> operators2 = new ArrayList<String>(Arrays.asList("*", "/"));
        boolean isOperator = lexeme.getType() == Lexeme.Type.OPERATOR; 
        return isOperator && operators2.contains(lexeme.getVal());
    }

}

final class NodeFactory {
    public static Node createConst(int curLexeme, List<Lexeme> list) {
        return new Node(Node.Type.CONST, list.get(curLexeme).getVal());
    }
    
    public static Node createOp(int curLexeme, List<Lexeme> list) {
        return new Node(Node.Type.OP, list.get(curLexeme).getVal());
    }

    public static Node createVar(int curLexeme, List<Lexeme> list) {
        return new Node(Node.Type.VAR, list.get(curLexeme).getVal());
    }
    
    public static Node createUnar(int curLexeme, List<Lexeme> list) {
        return new Node(Node.Type.UNAR, list.get(curLexeme++).getVal());
    }
}