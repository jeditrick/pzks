package net.sukharevd.cssw.model.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sukharevd.cssw.model.entities.Node;

class Level {
    private List<Node> nodes = new ArrayList<Node>();
    public List<Node> getNodes() {
        return nodes;
    }
}

public class LevelSeparator {
    
    public Map<Node, int[]> positions = new HashMap<Node, int[]>();
    public List<Level> levels = new ArrayList<Level>();
    public void exploreLevels(Node curNode, int curLevel) {
        if (curNode == null) {
            return;
        }
        if (curLevel >= levels.size()) {
            levels.add(new Level());
        }
        // add curNode
        levels.get(curLevel).getNodes().add(curNode);
        int posX = curLevel;
        int posY = levels.get(curLevel).getNodes().size() - 1;
        positions.put(curNode, new int[] {posX, posY});
        // take care about children
        exploreLevels(curNode.getLeftChild(), curLevel + 1);
        exploreLevels(curNode.getRightChild(), curLevel + 1);
    }
}
