package net.sukharevd.cssw.model.logic.exception;

@SuppressWarnings("serial")
public class SyntaxAnalysisException extends Exception {
    public SyntaxAnalysisException(String message) {
        super(message);
    }
}
