package net.sukharevd.cssw.model.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.sukharevd.cssw.model.entities.Lexeme;
import net.sukharevd.cssw.model.logic.exception.LexicalAnalysisException;
import net.sukharevd.cssw.model.logic.exception.UnexpectedEOLException;
import net.sukharevd.cssw.model.logic.exception.UnexpectedSymbolException;

public class LexicalAnalyser {

    public enum CharClass {
        SPACE, OPEN_BR, CLOSE_BR, LET, NUM, DOT, OPERATION, UNAR, EOL
    }

    private enum VS {
        V0, V1, V2, V3, V4, V5, V6, V7, V8, E
    }

    private final static VS[][] TRANS = {
      //  S      OBR     CBR    LET    NUM    DOT    OP    UNAR   EOL
        {VS.V0, VS.V1,  VS.E, VS.V2, VS.V3,  VS.E,  VS.E,  VS.E, VS.E }, // VS.V0
        {VS.V1, VS.V1,  VS.E, VS.V2, VS.V3,  VS.E,  VS.E, VS.V8, VS.E }, // VS.V1
        {VS.V4,  VS.E, VS.V5, VS.V2, VS.V2,  VS.E, VS.V6,  VS.E, VS.V2}, // VS.V2
        {VS.V4,  VS.E, VS.V5,  VS.E, VS.V3, VS.V7, VS.V6,  VS.E, VS.V3}, // VS.V3
        {VS.V4,  VS.E, VS.V5,  VS.E,  VS.E,  VS.E, VS.V6,  VS.E, VS.V4}, // VS.V4
        {VS.V5,  VS.E, VS.V5,  VS.E,  VS.E,  VS.E, VS.V6,  VS.E, VS.V5}, // VS.V5
        {VS.V0, VS.V1,  VS.E, VS.V2, VS.V3,  VS.E,  VS.E,  VS.E, VS.E }, // VS.V6
        {VS.V4,  VS.E, VS.V5,  VS.E, VS.V7,  VS.E, VS.V6,  VS.E, VS.V7}, // VS.V7
        {VS.V0, VS.V1,  VS.E, VS.V2, VS.V3,  VS.E,  VS.E,  VS.E, VS.E }  // VS.V8
    };

    private final List<VS> BUILT_TERM = new ArrayList<VS>(Arrays.asList(VS.V2, VS.V3)); 
    private final List<VS> NEMOY_TERM = new ArrayList<VS>(Arrays.asList(VS.V0, VS.V4, VS.V7)); 
    
    private VS curState;

    public List<Lexeme> parse(String str) throws LexicalAnalysisException {
        curState = VS.V0;
        List<Lexeme> list = new ArrayList<Lexeme>();
        Lexeme curLexeme = null;
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            CharClass c = determineClass(ch, i);
            VS newState = TRANS[curState.ordinal()][c.ordinal()];
            if (newState == VS.E) {
                throw new UnexpectedSymbolException(ch, i);
            }
            if (!(newState == curState && BUILT_TERM.contains(newState))
                    && c != CharClass.SPACE && !NEMOY_TERM.contains(newState)) {
                curLexeme = createLexeme(ch, newState, i);
                list.add(curLexeme);
            } else if (newState == curState && BUILT_TERM.contains(newState)) {
                curLexeme.addChar(ch);
            }
            //System.out.print(curState + ", ");
            curState = newState;
        }
        checkEndSpil();
        //System.out.println("\nAmount of lexems: " + list.size());
        return list;
    }

    private void checkEndSpil() throws UnexpectedEOLException {
        if (TRANS[curState.ordinal()][CharClass.EOL.ordinal()] == VS.E) {
            throw new UnexpectedEOLException();
        }
    }

    private CharClass determineClass(char ch, int pos) throws UnexpectedSymbolException {
        if (Character.isWhitespace(ch)) {
            return CharClass.SPACE;
        } else if (ch == '(') {
            return CharClass.OPEN_BR;
        } else if (ch == ')') {
            return CharClass.CLOSE_BR;
        } else if (Character.isLetter(ch)) {
            return CharClass.LET;
        } else if (Character.isDigit(ch)) {
            return CharClass.NUM;
        } else if (ch == '.') {
            return CharClass.DOT;
        } else if (curState == VS.V1 && ch == '-') {
            return CharClass.UNAR;
        } else if (ch == '+' || ch == '-' || ch == '*' || ch == '/') {
            return CharClass.OPERATION;
        }
        throw new UnexpectedSymbolException(ch, pos);
    }
    
    private Lexeme createLexeme(char ch, VS newState, int i) {
        switch (newState) {
            case V1: return new Lexeme(Lexeme.Type.OP_BR, ch, i);
            case V5: return new Lexeme(Lexeme.Type.CL_BR, ch, i);
            case V2: return new Lexeme(Lexeme.Type.VAR, ch, i);
            case V3: return new Lexeme(Lexeme.Type.CONST, ch, i);
            case V6: return new Lexeme(Lexeme.Type.OPERATOR, ch, i);
            case V8: return new Lexeme(Lexeme.Type.UNAR, ch, i);
            default: throw new IllegalStateException("Internal error. Unexpected new state + " + newState);
        }
    }
    
}