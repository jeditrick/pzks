package net.sukharevd.cssw.model.logic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.entities.Node.Type;

public class Shuffler {

    public void shuffle(List<Integer> curSequence, List<Integer> numbers,
            List<List<Integer>> resultSequences, List<Integer> shuttledTermsList, LinkedList<Node> list,
            int minNumber) {
        if (numbers.size() == minNumber && !isInverted(curSequence, resultSequences)) {
            resultSequences.add(curSequence);
        }
        
//        List<Integer> curClone = new ArrayList<Integer>(curSequence);
        for (int i = numbers.size() -1; i >= 0; i--) {
            if (isPlain(list, shuttledTermsList, numbers, i)) {
                curSequence.add(0, numbers.get(i));
                numbers.remove(i);
                //minNumber++;
                if (numbers.size() == minNumber && !isInverted(curSequence, resultSequences)) {
                    resultSequences.add(curSequence);
                }
            }
        }
        
        for (int i = minNumber; i < numbers.size(); i++) {
            if (isDistinguishable(numbers, i, list, shuttledTermsList))
            {
                List<Integer> curClone = new ArrayList<Integer>(curSequence);
                curClone.add(numbers.get(i));
                numbers.remove(i);
                shuffle(curClone, numbers, resultSequences, shuttledTermsList, list, minNumber);
                numbers.add(i, curClone.get(curClone.size()-1));
            }
        }
    }

    private boolean isPlain(LinkedList<Node> list,  List<Integer> shuttledTermsList, List<Integer> numbers, int i) {
        Node n = list.get(shuttledTermsList.get(numbers.get(i)));
        return (n.weight() == 0); 
    }

    private boolean isDistinguishable(List<Integer> numbers, int curNum, LinkedList<Node> list,
            List<Integer> shuttledTermsList) {
        for (int i = 0; i < curNum; i++) {
            Node n1 = list.get(shuttledTermsList.get(i));
            Node n2 = list.get(shuttledTermsList.get(numbers.get(curNum)));
            Node n3 = getOperatorFor(i, shuttledTermsList, list);
            Node n4 = getOperatorFor(numbers.get(curNum), shuttledTermsList, list);
            if (n1.weight() == 0 && n1.weight() == n2.weight()) {
                return false;
            }
         // (a+b)*c+2+7/x+m+n*s-y+(f-2+z)+e
            if (n1.toOperationsString().equals(n2.toOperationsString()) && n3.toOperationsString().equals(n4.toOperationsString())) {
                return false;
            }
        }
        return true;
    }

    private Node getOperatorFor(int i, List<Integer> shuttledTermsList, LinkedList<Node> list) {
        Integer idx = shuttledTermsList.get(i);
        if (idx == 0 /*&& list.size() > 1*/) {
            if ("+".equals(list.get(1).getValue()) || "-".equals(list.get(1).getValue())) {
                return new Node(Type.OP, "+");
            } else if ("*".equals(list.get(1).getValue()) || "/".equals(list.get(1).getValue())) {
                return new Node(Type.OP, "*");
            }
//            System.out.println("Unexpected node: " + list.get(idx-1));
//            return list.get(idx-1);
        } else {
            return list.get(idx-1);
        }
        return null;
    }

    private boolean isInverted(List<Integer> curSequence, List<List<Integer>> resultSequences) {
        l1:
        for (int i = 0; i < resultSequences.size(); i++) {
            if (resultSequences.get(i).size() != curSequence.size()) continue;
            for (int j = 0; j < resultSequences.get(i).size(); j++) {
                if (!resultSequences.get(i).get(j).equals(curSequence.get(curSequence.size()-1-j))) {
                    continue l1;
                }
            }
            return true;
        }
        return false;
    }

}
