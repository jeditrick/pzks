package net.sukharevd.cssw.model.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.sukharevd.cssw.model.entities.Node;
import net.sukharevd.cssw.model.entities.Node.Type;

public class MatrixSystemEmulator {
    
    private final static int P = 3;
    
    private LevelSeparator ls = new LevelSeparator();

    private int ts;

    private double ky;

    private double ke;

    private int tp;

    private int levelsAmount;

    private List<Integer[]> utility;

    private int maxWidth;
    
    public void execute(Node curNode) {
        System.out.println(curNode);
        ls.exploreLevels(curNode, 0);
        List<Node> ready = new ArrayList<Node>();
        Set<Node> allNodes = ls.positions.keySet();
        utility = new ArrayList<Integer[]>();
        int j = 1;
        for (Node node : allNodes) {
            node.id = j++;
        }
        // a+b+c+d-e*f*g*(h+i)
        
        // 1. add all const, var, unar operators to the ready-list
        for (Node node : allNodes) {
            if ( (node.getType() == Type.CONST) || (node.getType() == Type.VAR) || (node.getType() == Type.UNAR)) {
                ready.add(node);
                node.isDone = true;
            }
        }
        levelsAmount = 0;
        tp = 0;
        maxWidth = Integer.MIN_VALUE;
        while (!(ready.size()==1 && ready.get(0).getParent()==null)) {
        // 2. Delete done nodes, try to replace them with their parent
        for (int i = ready.size() - 1; i >= 0; i--) {
            Node victim = ready.get(i);
            if (victim.isDone) {
                ready.remove(i);
                if (victim.getParent() == null) throw new IllegalStateException();
                if (victim.getParent().getLeftChild().isDone && victim.getParent().getRightChild().isDone) {
                    ready.remove(victim.getParent().getLeftChild());
                    ready.remove(victim.getParent().getRightChild());
                    ready.add(victim.getParent());
                }
            }
            victim = null;
        }
        // a+b*c+e*(k+p)
        // 3. Sort ready by level
        sortByLevels(ready);
                
        // 4. Count number of each operation type (+, -, *, /)
        int[] counters = new int[4];
        counters[0] = 0; counters[1] = 0; counters[2] = 0; counters[3] = 0;
        for (Node node : ready) {
            if ("+".equals(node.getValue().toString().trim())) {
                counters[0]++;
            } else if ("-".equals(node.getValue().toString().trim())) {
                counters[1]++;
            } else if ("*".equals(node.getValue().toString().trim())) {
                counters[2]++;
            } else if ("/".equals(node.getValue().toString().trim())) {
                counters[3]++;
            }
        }
        final String[] OP = { "+", "-", "*", "/" };
        int maxVal = -1;
        int maxIdx = -1;
        for (int i = 0; i < counters.length; i++) {
            if (maxVal < counters[i]) {
                maxVal = counters[i];
                maxIdx = i;
            }
        }
        final String resOp = OP[maxIdx];

        List<Node> resList = new ArrayList<Node>();
        for (Node node : ready) {
            if (node.getValue().toString().trim().equals(resOp)) {
                resList.add(node);
            }
        }

        // Push operations
        Integer[] row = new Integer[P];
        int amountToBePushed = Math.min(P, resList.size());
        maxWidth = Math.max(maxWidth, resList.size());
        if (resList.isEmpty()) System.out.println(curNode + " | " + resList + " | " + resOp  + " | " + ready + " | " + allNodes);
        System.out.print(resList.get(0).getValue().toString().trim() + ": ");
        
        for (int i = 0; i < amountToBePushed; i++) {
            System.out.print(resList.get(i).id + " ");
            resList.get(i).isDone = true;
            row[i] = resList.get(i).id;
        }
        System.out.println();
        for (int i = 0; i < weigth(resOp); i++) {
            utility.add(row);
        }
        
        tp += weigth(resOp);
        levelsAmount++;
        }
        
        ts = curNode.weight();
        ky = (double)ts/tp;
        ke = (double)ts/tp/P;
    }
    
    private void sortByLevels(List<Node> ready) {
        boolean isSort;
        do {
            isSort = true;
            for (int i = 0; i < ready.size() - 2; i++) {
                if (levelOf(ready.get(i)) < levelOf(ready.get(i+1))) {
                    Node temp = ready.get(i);
                    ready.set(i, ready.get(i+1));
                    ready.set(i+1, temp);
                    isSort = false;
                }
            }
        } while (!isSort);
    }

    private int levelOf(Node node) {
        for (int i = 0; i < ls.levels.size(); i++) {
            if (ls.levels.get(i).getNodes().contains(node)) {
                return i;
            }
        }
        throw new IllegalArgumentException();
    }

    private int weigth(String resOp) {
        if ("+".equals(resOp.trim())) {
            return 1;
        } else if ("-".equals(resOp.trim())) {
            return 2;
        } else if ("*".equals(resOp.trim())) {
            return 3;
        } else if ("/".equals(resOp.trim())) {
            return 4;
        }
        throw new IllegalArgumentException();
    }

    public int getTs() {
        return ts;
    }

    public double getKy() {
        return ky;
    }

    public double getKe() {
        return ke;
    }

    public int getTp() {
        return tp;
    }

    public int getLevelsAmount() {
        return levelsAmount;
    }

    public List<Integer[]> getUtility() {
        return utility;
    }
    
    public String getGant() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < utility.get(0).length; i++) {
            sb.append("P").append(i).append("\t");
        }
        sb.append("\n");
        for (Integer[] row : utility) {
            for (Integer value : row) {
                sb.append((value==null)?"-":value).append("\t");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public int getMaxWidth() {
        return maxWidth;
    }
    
}
