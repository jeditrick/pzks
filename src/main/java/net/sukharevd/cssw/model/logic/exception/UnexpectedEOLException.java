package net.sukharevd.cssw.model.logic.exception;

@SuppressWarnings("serial")
public class UnexpectedEOLException extends LexicalAnalysisException {
    public UnexpectedEOLException() {
        super("Unexpected end of line");
    }
}
