package net.sukharevd.cssw.model.logic.exception;

import net.sukharevd.cssw.model.entities.Lexeme;

@SuppressWarnings("serial")
public class UnexpectedLexemeException extends SyntaxAnalysisException {

    public UnexpectedLexemeException(Lexeme lexeme) {
        super("Unexpected symbol '" + lexeme.getVal()
                + "' at " + lexeme.getPlace());
    }

}
