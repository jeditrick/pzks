package net.sukharevd.cssw.model.logic.exception;

@SuppressWarnings("serial")
public class MissingClosingBracketException extends SyntaxAnalysisException {
    public MissingClosingBracketException() {
        super("Missing ')'");
    }
}
