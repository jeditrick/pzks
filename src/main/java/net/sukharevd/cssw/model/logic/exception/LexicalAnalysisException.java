package net.sukharevd.cssw.model.logic.exception;

@SuppressWarnings("serial")
public class LexicalAnalysisException extends Exception {
    public LexicalAnalysisException(String message) {
        super(message);
    }
}
